# SurvClust

Semi-supervised clustering of samples into 'poor' and 'good' prognosis groups.

#####Example:
```r
## Define pipeline to be run on all datasets
Run <- function(eSet, name)
{
    message('Running analysis on dataset: ', name)

    ## Rank genes by cox proportional hazard regression model "scores"
    genes <- TopCoxGenes(eSet)

    ## Identify the classifier that yields the most optimal
    ## stratification of samples
    classifier <- BestClassifier(eSet, genes)

    ## Draw kaplan meier plots
    PlotKM(Time(eSet), Event(eSet), classifier$groups,
           filename = sprintf('%s_km_plot.pdf', name))

    ## Differential expression analysis
    de <- DiffEq(eSet, classifier$groups, name, poor - good)[[1]]

    message('Done analysing dataset: ', name)

    ## Return results
    list(classifier = classifier, de = de)
}


##
## Main
##

library(survclust)

## Source datasets
library(brca)

data(ispy1)
data(gse25066)
data(yau)
data(chin)

## Prepare a list of datasets
datasets <- list(
    ispy1     = ExpressionSet2(ispy1,
                               timeCol = 'rfs.t',
                               eventCol = 'rfs.e'),
    gse25066  = ExpressionSet2(gse25066,
                               timeCol = 'drfs_t',
                               eventCol = 'drfs_e'),
    yau       = ExpressionSet2(yau,
                               timeCol = 't_dmfs',
                               eventCol = 'e_dmfs'),
    chin      = ExpressionSet2(chin,
                               timeCol = 'disease_time',
                               eventCol = 'disease_binary')
    )

## Run pipeline on datasets
res <- mapply(Run, datasets, names(datasets), SIMPLIFY = FALSE)

## Identify genes that are commonly DE between "poor" and "good" prognosis
## groups in at least 3 datasets
common <- CommonDEGs.MC(lapply(res, '[[', 'de'), inAtLeast = 3,
                        iterations = 100000, holm < 0.05)
```
